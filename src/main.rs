// -*- compile-command: "cargo run" -*-

use std::fs::File;
fn density(arr: &mut [u8]) {
    let density_arr: [char; 27] = [
        'Ñ', '@', '#', 'W', '$', '9', '8', '7', '6', '5', '4', '3', '2', '1', '0', '?', '!', 'a',
        'b', 'c', ';', ':', '+', '=', ',', '.', '-',
    ];
    for i in 0..arr.len() - 1 {
        let num: f32 = (i as f32) / 9.0;

        // println!("the value  is {:?}", num.floor() as usize);
        if num.floor() > 26.0 {
            arr[i] = density_arr[26] as u8;
        } else {
            arr[i] = density_arr[num.floor() as usize] as u8;
        }
    }
    println!("the arr is {:?}", arr);
}

fn main() {
    // The decoder is a build for reader and can be used to set various decoding options
    // via `Transformations`. The default output transformation is `Transformations::IDENTITY`.
    let decoder = png::Decoder::new(File::open("../assets/logo-icon-white.png").unwrap());

    // let info = decoder.read_header_info().unwrap();

    let mut reader = decoder.read_info().unwrap();
    println!("the output buffer size is {}", reader.output_buffer_size());
    let mut arr: [u8; 256] = [0; 256];
    density(&mut arr);

    let info = reader.info();
    let width = info.width;
    let height = info.height;
    // println!("the width is {}", info.width);
    // Allocate the output buffer.
    let mut buf = vec![0; reader.output_buffer_size()];
    // Read the next frame. An APNG might contain multiple frames.
    let _info = reader.next_frame(&mut buf).unwrap();
    for i in 0..width {
        for j in 0..height {
            print!(
                "{}",
                arr[buf[((i + (j * width)) as usize)] as usize] as char
            );
        }
        println!();
    }
}
